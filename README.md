# yii1-array-validator
A validator that validates the content of an array, used in combination with other validators.

## Installation

The installation of this library is made via composer.
Download `composer.phar` from [their website](https://getcomposer.org/download/).
Then add to your composer.json :

```json
	"require": {
		...
		"yii1-extended/yii1-array-validator": "^1",
		...
	}
```

Then run `php composer.phar update` to install this library.
The autoloading of all classes of this library is made through composer's autoloader.

## Basic Usage

In the model validation :

```php

public function rules()
{
	return array(
		...
		array('<attribute names>', 'CArrayValidator', 'rules' => array(
			...
			array('CNumberValidator', 'allowEmpty' => true),
			... 
		),
		...
	);
}

```

This validator validates an array represented by the attribute it gets as a
parameter against all the validators that are stored into its `rules` attribute.
The rules attribute should be formatted like the whole `rules()` method of
the record. Nested `CArrayValidator` are applicable, if the structure to
validate is a regular structure (like a 3d matrix).

Remember that this validator validates only the values, not the keys of such
array.

When the rules for the inner validators are created, their 0th attribute (a.k.a.
the list of attributes for which this validator will validate against) is not
needed anymore because all the inner validators will check all the values of
the array to validate. Thus, their new 0th attribute should be their classname,
or built-in validator name.

## License

MIT (See [license file](LICENSE)).
