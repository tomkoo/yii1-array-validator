<?php

/**
 * CArrayValidator class file.
 *
 * This validator validates the content of an array, by making others validators
 * to run.
 *
 * @author Anastaszor
 */
class CArrayValidator extends CValidator
{
	
	/**
	 * An expected minimum number of elements (inclusive) in the array.
	 * Defaults to null, meaning no minimum number of elements. If minElements
	 * is defined and equals to 0, it will validate any array.
	 *
	 * @var integer
	 */
	public $minElements = null;
	
	/**
	 * An expected maximum number of elements (inclusive) in the array.
	 * Defaults to null, meaning no maximum number of elements. If maxElements
	 * is defined and equals to 0, il will deny any non-empty array.
	 *
	 * @var integer
	 */
	public $maxElements = null;
	
	/**
	 * The rules to validate against. Those rules must be formatted like the
	 * CModel::rules() method, but DOES NOT NEED the first argument (the field
	 * list to be validated) as that list is already specified with this
	 * validator. Thus, any rule line must have a [0] argument with is the
	 * name of the validator (inline validator or classname of a validator),
	 * then all others params will be treated as is for a new validator.
	 *
	 * @var mixed[]
	 * @see CModel::rules()
	 */
	public $rules = array();

    /**
     * @var boolean whether the attribute value can be null or empty. Defaults to true,
     * meaning that if the attribute is empty, it is considered valid.
     */
    public $allowEmpty=true;

	/**
	 * {@inheritDoc}
	 * @see CValidator::validateAttribute()
	 */
	public function validateAttribute($object, $attribute)
	{
		$array_to_validate = $object->$attribute;

        if($this->allowEmpty && $this->isEmpty($array_to_validate))
            return;

		if(!is_array($array_to_validate))
		{
			$this->addError($object, $attribute, Yii::t('validator.array',
				'The attribute "{attr}" is not an array.', array(
				'{attr}' => $attribute,
			)));
			return;
		}
		
		if($this->minElements !== null && count($array_to_validate) < $this->minElements)
		{
			$this->addError($object, $attribute, Yii::t('validator.array',
				'{attribute} should contains at least {n} elements.',
				array('{n}' => $this->minElements)
			));
			return;
		}
		
		if($this->maxElements !== null && count($array_to_validate) > $this->maxElements)
		{
			$this->addError($object, $attribute, Yii::t('validator.array',
				'{attribute} should contains at max {n} elements.',
				array('{n}' => $this->maxElements)
			));
			return;
		}
		
		foreach($this->rules as $rule)
		{
			$name = $rule[0];
			unset($rule[0]);
			$validated_values = array();
			// extract the values to validate from object
			foreach($array_to_validate as $key => $value)
			{
				$object->$attribute = $value;
				// for each value, set the attribute at this specific value
				$validator = CValidator::createValidator($name, $object, array($attribute), $rule);
				$validator->validate($object, array($attribute));
				// get the validated values into another array
				$validated_values[$key] = $object->$attribute;
			}
			// set back the attribute array with the validated attributes
			$object->$attribute = $validated_values;
		}
	}
	
}
